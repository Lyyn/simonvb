﻿Imports System.Threading
Imports System.Net.Sockets
Imports System.Net

Module ServerThread
    Dim listeNotes As New List(Of Integer)()     ' Liste de notes envoyée aux joueurs
    Dim listeClients As New List(Of Client)      ' Contient la liste de tous les joueurs (2 maximum donc)
    Dim randomMachine As New Random              ' Utilisé pour générer les nombres aléatoires de notes

    Dim port As String = "1337"                  ' Port utilisé pour la communication (doit correspondre au port côté client)

    Dim packet(255) As Byte                      ' Envoi/Réception d'un packet forgé
    Dim forgePacket As List(Of String)           ' Construction d'un packet
    Dim readPacketStr As String                  ' Déconstruction d'un packet
    Dim readPacket() As String                   ' Mise en liste d'un packet (Type = 0, Contenu = 1)

    Dim threadClient As Thread                   ' Chaque client possède son propre thread séparé pour les opérations bloquantes
    Dim socketServer As Socket                   ' Socket d'écoute du serveur
    Dim socketListen As Socket                   ' Socket d'écoute des clients (chaque client aura son propre socket)

    ' > Debug
    Dim verbose As Boolean = False

    Public Sub StartServer()
        ' Démarrage du serveur et mise sur écoute
        socketServer = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        Dim endPoint As IPEndPoint = New IPEndPoint(IPAddress.Any, port)
        If verbose Then Debug.Print("[SERVER] Lancement du serveur")

        ' Lancement du socket d'écoute et génération des premières notes qui seront jouées
        socketServer.Bind(endPoint)
        socketServer.Listen(1)
        listeNotes.Add(randomMachine.Next(4))
        If verbose Then Debug.Print("[SERVER] Serveur en écoute sur le port " & port)

        ' Attente des clients
        While (listeClients.Count < 2)
            Dim accepted As Boolean = True  ' Flag : Vérifier que le socket est bien en écoute
            If verbose Then Debug.Print("[SERVER] " & listeClients.Count & " client(s) connectés. En attente...")

            Try
                socketListen = socketServer.Accept()
            Catch ex As Exception
                accepted = False
            End Try

            ' Le serveur est bien en écoute
            If accepted Then
                If verbose Then Debug.Print("[SERVER] Demande de connexion reçue")
                ConnecterClient(socketListen)

                ' Le joueur présent a lancé le serveur, on lui indique d'attendre
                If listeClients.Count = 1 Then
                    forgePacket = New List(Of String)
                    forgePacket.Add(PacketType.WAITPLAYER)
                    forgePacket.Add(PacketType.DUMMY)

                    listeClients(0).SendPacket(NetStrEncode(forgePacket))
                End If

                ' Un deuxième objet joueur a été créé, mais si le serveur a été lancé deux fois sur
                ' la même machine, le client peut ne pas exister. On vérifie.
                If listeClients.Count = 2 Then

                    ' On attend le traitement du client
                    While IsNothing(listeClients(1).exist)
                        If verbose Then Debug.Print("[SERVER] Attente du traitement du client...")
                        Thread.Sleep(50)        ' Boucle de 50 ms en attente du traitement du client reçu
                    End While

                    If Not listeClients(1).exist Then
                        ' Il n'existe pas réellement (ex. : création de serveur sur la machine hébergeant déjà un serveur)
                        ' on le supprime de la liste et on relance la boucle du serveur
                        If verbose Then Debug.Print("[SERVER] Un deuxième serveur a voulu être lancé sur cette machine. Il a été intercepté.")

                        listeClients.RemoveAt(1)
                        socketListen.Close()
                        socketListen = socketServer.Accept() ' Rafraîchir le socket pour éviter une connexion qui se ferait quand même.
                    End If
                End If
            End If
        End While

        If verbose Then Debug.Print("[SERVER] Deux clients sont connectés, mise en route de la partie")
        ' Informer les joueurs du nom de leur adversaire
        SendNames()

        ' Deux clients connectés : début de la partie
        Dim inGame As Boolean = True    ' Boucle du jeu
        Dim p1turn As Boolean = True    ' True : tour du joueur 1, False : tour du joueur 2

        While inGame
            If verbose Then Debug.Print("[SERVER] IN GAME WHILE")
            If p1turn Then listeNotes.Add(randomMachine.Next(4))   ' Ajouter une nouvelle note à chaque tour du joueur 1 (nouveau round donc)

            ' Lancement du tour
            forgePacket = New List(Of String)
            packet.Initialize()     ' Rafraîchir le tampon de lecture


            ' Envoi des notes au joueur qui a son tour
            forgePacket.Add(PacketType.NOTESLISTE)
            forgePacket.AddRange(NetNoteListEncode(listeNotes))

            Dim debugStr As String = ""
            For Each foo As String In forgePacket
                debugStr = debugStr + " " + foo
            Next
            If verbose Then Debug.Print("[SERVER] packet note :" & debugStr)

            listeClients(CurrentPlayer(p1turn)).SendPacket(NetStrEncode(forgePacket))


            ' Envoi du packet d'attente au deuxième
            forgePacket = New List(Of String)
            forgePacket.Add(PacketType.WAIT)
            listeClients(WaitingPlayer(p1turn)).SendPacket(NetStrEncode(forgePacket))

            ' Attente du tour du joueur en cours (bloquant)
            packet = listeClients(CurrentPlayer(p1turn)).ReceivePacket()
            readPacketStr = NetStrDecode(packet)
            readPacket = readPacketStr.Split(PacketType.SEPCHAR)
            readPacket(readPacket.Count - 1) = readPacket(readPacket.Count - 1).Replace(vbNullChar, "")
            Debug.Print("[SERVER - readPacket]" & readPacketStr)

            ' Packet reçu, 2 possibilités : réussite ou fail.
            If readPacket(0) = "" Then
                ' Packet vide, client déco.
                inGame = False
                forgePacket = New List(Of String)
                forgePacket.Add(PacketType.FLEEAWAY)
                forgePacket.Add(PacketType.DUMMY)
                listeClients(WaitingPlayer(p1turn)).SendPacket(NetStrEncode(forgePacket))
                socketServer.Close()
            Else
                ' Packet non vide, on le traite
                If readPacket(0) = PacketType.FAILED Then
                    If verbose Then Debug.Print("[SERVER] Packet FAILED reçu (" & WhoIsPlaying(p1turn) & ")")
                    ' Le joueur en cours a raté une note, c'est la fin du jeu
                    inGame = False

                    ' Envoi de la notif de victoire à l'autre joueur
                    forgePacket = New List(Of String)
                    forgePacket.Add(PacketType.VICTORY)
                    forgePacket.Add(PacketType.DUMMY)
                    listeClients(WaitingPlayer(p1turn)).SendPacket(NetStrEncode(forgePacket))
                    socketServer.Close()

                    ' Le joueur adversaire a quitté la partie
                ElseIf readPacket(0) = PacketType.FLEEAWAY Then
                    If verbose Then Debug.Print("[SERVER] Packet FLEEAWAY reçu (" & WhoIsPlaying(p1turn) & ")")
                    forgePacket = New List(Of String)
                    forgePacket.Add(PacketType.FLEEAWAY)
                    forgePacket.Add(PacketType.DUMMY)
                    listeClients(WaitingPlayer(p1turn)).SendPacket(NetStrEncode(forgePacket))
                    socketServer.Close()


                Else
                    Debug.Print("[SERVER] Packet non géré reçu (" & WhoIsPlaying(p1turn) & ")")
                    ' On change de joueur pour éviter une boucle infinie
                    p1turn = Not p1turn

                End If
            End If

        End While
    End Sub


    ' Renvoie l'indice du joueur en cours
    Private Function CurrentPlayer(p1turn As Boolean) As Integer
        Return IIf(p1turn, 0, 1) ' Iif = équivalent de l'opérateur ternaire en C ( x ? foo : bar)
    End Function


    ' Renvoie l'indice du joueur qui attend
    Private Function WaitingPlayer(p1turn As Boolean) As Integer
        Return IIf(p1turn, 1, 0)
    End Function


    ' Debug only
    Private Function WhoIsPlaying(p1turn As Boolean) As String
        If p1turn Then
            Return "Joueur Hôte"
        Else
            Return "Joueur distant"
        End If

    End Function


    ' Fonction qui se charge d'échanger les noms des joueurs
    Private Sub SendNames()
        ' Envoi du nom du joueur invité au joueur hôte
        If verbose Then Debug.Print(listeClients(0).getPseudo & " vs " & listeClients(1).getPseudo)

        forgePacket = New List(Of String)
        forgePacket.Add(PacketType.PSEUDOADV)
        forgePacket.Add(listeClients(1).getPseudo())
        listeClients(0).SendPacket(NetStrEncode(forgePacket))

        ' Envoi du nom du joueur hôte au joueur invité
        forgePacket = New List(Of String)
        forgePacket.Add(PacketType.PSEUDOADV)
        forgePacket.Add(listeClients(0).getPseudo())
        listeClients(1).SendPacket(NetStrEncode(forgePacket))
    End Sub


    Sub ConnecterClient(SocketEnvoi As Socket)
        If verbose Then Debug.Print("[SERVER] Connexion d'un client")

        Dim newClient As New Client(SocketEnvoi)
        listeClients.Add(newClient)
        threadClient = New Thread(AddressOf newClient.makeClient)
        threadClient.Start()
    End Sub


    Sub killSocketListen()
        Try
            Thread.Sleep(1)
            socketListen.Close()
        Catch ex As Exception

        End Try
    End Sub

    ' Fermer les sockets et les threads
    Delegate Sub dgKill()
    Public Sub Kill()
        socketServer.Close()
        socketListen.Close()

        Try
            listeClients(0).Close()
        Catch ex As Exception
        End Try

        Try
            listeClients(1).Close()
        Catch ex As Exception
        End Try

        Try
            Dim killStuckedSocket As New Thread(AddressOf killSocketListen)     ' Il arrive que le socket soit bloqué, on le kill via un autre thread
        Catch ex As Exception

        End Try


        Try
            socketServer.Close()
        Catch ex As Exception

        End Try

        Try
            threadClient.Abort()
        Catch ex As Exception
        End Try
    End Sub
End Module
