﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Media

Class MainWindow
    ' > Config
    Dim ip As String                                ' IP du serveur (champ de l'UI)
    Dim port As String = "1337"                     ' Port du serveur (fixe)
    Dim randomMachine As New Random                 ' Générer un nom de joueur aléatoire (ex. : Anonyme42)
    Dim imagesList As New List(Of Image)            ' Contient les références vers les images du Simon (voir les Resources dans My Project)

    ' > Socket
    Dim socketClient As Socket
    Dim endPointClient As IPEndPoint
    Dim packet As Byte()

    ' > Threads
    Dim threadServer As Thread
    Dim threadClient As Thread
    Dim threadSon(4) As Thread                      ' Jouer les sons async

    ' > Jeu
    Dim canPlay As Boolean = False                  ' Si true : les clics sur les images seront interprétés (à des fins de debug ; BLOQUE LE JEU.)
    Dim spList(4) As SoundPlayer                    ' Instancie les sons et permet leur lecture
    Dim currentNotesList As New List(Of Integer)    ' Liste des notes à jouer (reçue du serveur)
    Dim currentNote As Integer = 0                  ' Permet de savoir quelle sera la prochaine note à jouer

    ' > Debug
    Dim verbose As Boolean = False


    ' Faire quelque chose quand la fenêtre est chargée
    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)

        ' Nom random pour le joueur
        TB_pseudo.Text = TB_pseudo.Text & randomMachine.Next(1000)

        ' Ajout des références des Image du GUI dans une liste prévue à cet effet
        imagesList.Add(image1b)
        imagesList.Add(image2b)
        imagesList.Add(image3b)
        imagesList.Add(image4b)

        ' Sons
        spList(0) = New SoundPlayer(My.Resources._0)
        spList(1) = New SoundPlayer(My.Resources._1)
        spList(2) = New SoundPlayer(My.Resources._2)
        spList(3) = New SoundPlayer(My.Resources._3)

        ' Debug
        If verbose Then Debug.Print("[GUI] Window Loaded")

    End Sub


    Private Sub B_heberger_Click(sender As Object, e As RoutedEventArgs) Handles B_heberger.Click
        DisableUI()         ' Empêcher le joueur de relancer des connexions tant qu'il joue
        LaunchServer()      ' Lancer le serveur puis le client
        LaunchClient()
    End Sub


    Private Sub B_rejoindre_Click(sender As Object, e As RoutedEventArgs) Handles B_rejoindre.Click
        If TB_pseudo.Text = "" Then
            MsgBox("Veuillez entrer un pseudonyme")
        ElseIf TB_ip.Text = "" Then
            MsgBox("Veuillez entrer une IP")
        Else
            ' Conditions OK, connexion !
            DisableUI()
            LaunchClient(TB_ip.Text)
        End If

    End Sub


    ' Lancer le Thread du server
    Private Sub LaunchServer()
        ' Tester la disponibilité du port
        Dim launchServer As Boolean = True      ' Si false : impossible de lancer le serveur, donc avertissement
        Dim endPointServer As IPEndPoint
        Dim socketServer As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

        Try
            endPointServer = New IPEndPoint(IPAddress.Parse("127.0.0.1"), port)
            socketServer.Connect(endPointServer)
            launchServer = False
        Catch ex As Exception
        End Try

        ' Si le port est dispo, on le libère pour le vrai serveur
        socketServer.Close()

        If launchServer Then
            threadServer = New Thread(AddressOf StartServer)
            threadServer.Start()    ' Lance le processus via la fonction StartServer (Module ServerThread.vb)
        Else
            MsgBox("Impossible de démarrer le serveur. Le port " & port & " est déjà occupé par une autre application.")
            EnableUI()
        End If
    End Sub


    ' Méthode se chargeant du lancement du jeu
    Private Sub LaunchClient(Optional serverIp As String = "127.0.0.1")
        If verbose Then Debug.Print("[CLIENT] Lancement de la connexion au serveur " & serverIp)
        socketClient = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

        ' On tente de se connecter au serveur
        Try
            endPointClient = New IPEndPoint(IPAddress.Parse(serverIp), port)
            socketClient.Connect(endPointClient)
            LaunchGame()
        Catch ex As Exception
            MsgBox("Erreur de connexion au serveur. Vérifiez que le serveur soit accessible et que l'IP est correcte.")
            Disconnection()
        End Try

    End Sub


    Private Sub LaunchGame()
        ' On est connecté au serveur, première chose à faire : envoyer le pseudo puis attendre une réponse
        Dim forgePacket As New List(Of String)

        forgePacket.Add(PacketType.PSEUDO)
        forgePacket.Add(TB_pseudo.Text)
        packet = NetStrEncode(forgePacket)
        socketClient.Send(packet)

        ' Boucle du jeu dans un nouveau thread
        threadClient = New Thread(AddressOf GameLoop)
        threadClient.Start()
    End Sub


    ' Boucle du jeu
    Private Sub GameLoop()
        Dim _continue As Boolean = True
        Dim status As Integer       ' Status dans la partie

        ' Cette boucle tient en vie le jeu et le thread qui l'exécute
        While _continue
            Dim recPacket(255) As Byte

            If verbose Then Debug.Print("[CLIENT] Game Loop")

            If (Not socketClient.Connected) Then
                _continue = False
            Else
                Try
                    socketClient.Receive(recPacket)
                Catch ex As Exception
                    ' Peut se produire si la connexion est interrompue
                    Me.Dispatcher.Invoke(New dgDisconnection(AddressOf Disconnection))
                    Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "Connexion perdue avec l'autre joueur.")
                    _continue = False
                End Try
            End If

            ' Le socket du client est toujours en vie, on peut continuer à joeur
            If _continue Then
                Dim packetStr As String = NetStrDecode(recPacket)
                currentNotesList.Clear()

                ' Traitement du message reçu
                Dim packet() As String = packetStr.Split(PacketType.SEPCHAR)
                packet(packet.Count - 1) = packet(packet.Count - 1).Replace(vbNullChar, "")   ' Supprimer les vbNullChar en trop

                ' Selon le type de packet, un traitement spécifique
                Select Case packet(0)


                    Case PacketType.PSEUDOADV
                        If verbose Then Debug.Print("[CLIENT] Packet PSEUDOADV reçu")
                        'packet(1) = packet(1).Replace(vbNullChar, "")   ' Supprimer les vbNullChar en trop
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "Connecté à l'autre joueur : " & packet(1))


                    Case PacketType.NOTESLISTE
                        If verbose Then Debug.Print("[CLIENT] Packet NOTESLISTE reçu")
                        MsgBox("C'est à votre tour, soyez attentif !")
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "C'est à votre tour, soyez attentif !")

                        ' On joue !
                        Dim packetSub() As String = RmFirstItem(packet)
                        Dim notesListInt As New List(Of Integer)

                        For Each note As String In packetSub
                            Dim noteInt As Integer

                            Integer.TryParse(note, noteInt) ' convertir la note STR en note INT
                            notesListInt.Add(noteInt)
                        Next

                        ' Lancer la lecture des notes
                        currentNotesList = notesListInt
                        PlayNotes(notesListInt)


                    Case PacketType.WAIT
                        If verbose Then Debug.Print("[CLIENT] Packet WAIT reçu")
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "L'autre joueur effectue son tour.")


                    Case PacketType.WAITPLAYER
                        If verbose Then Debug.Print("[CLIENT] Packet WAITPLAYER reçu")
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "En attente de connexion de l'autre joueur")
                        status = PacketType.WAITPLAYER


                    Case PacketType.FLEEAWAY
                        If verbose Then Debug.Print("[CLIENT] Packet FLEEAWAY reçu")
                        MsgBox("L'autre joueur a quitté la partie.")
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "L'autre joueur a quitté la partie.")
                        Me.Dispatcher.Invoke(New dgDisconnection(AddressOf Disconnection))


                    Case PacketType.VICTORY
                        If verbose Then Debug.Print("[CLIENT] Packet VICTORY reçu")
                        MsgBox("Félicitations ! Vous avez gagné la partie !")
                        Me.Dispatcher.Invoke(New dgDisconnection(AddressOf Disconnection))
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "Félicitations ! Vous avez gagné la partie !")

                    Case Else
                        ' Packet d'un type non géré reçu : ne devrait pas arriver, on lance le debug et on coupe tout.
                        If verbose Then Debug.Print("[CLIENT] !!! Packet inconnu reçu !!!")
                        For Each pck As String In packet
                            If verbose Then Debug.Print("--> " & pck)
                        Next

                        Me.Dispatcher.Invoke(New dgDisconnection(AddressOf Disconnection))
                        Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "Connexion perdue avec l'autre joueur.")

                End Select
            End If
        End While
    End Sub


    ' Jouer la séquence de notes et laisser le joueur jouer.
    Private Sub PlayNotes(notesListe As List(Of Integer))
        ' On joue d'abord les notes (dans des thread dédiés pour éviter les problèmes)
        For Each note As Integer In notesListe
            Me.Dispatcher.Invoke(New dgEnableNote(AddressOf EnableNote), note)
            Me.Dispatcher.Invoke(New dgPressedNote(AddressOf pressedNote), note)
            Thread.Sleep(750)
            Me.Dispatcher.Invoke(New dgDisableNote(AddressOf DisableNote), note)
            Thread.Sleep(250)
        Next

        ' Maintenant on attend que le joueur fasse les notes dans le bon ordre
        canPlay = True
        currentNote = 0
    End Sub


    ' Modification du statut via un autre thread
    Delegate Sub dgChangeStatut(newStatut As String)
    Private Sub ChangeStatut(newStatut As String)
        L_status.Content = newStatut
    End Sub


    ' Lancer les procédures de déconnexion
    Delegate Sub dgDisconnection()
    Private Sub Disconnection()
        EnableUI()  ' Utile si la déconnexion ne survient pas de la fermeture de la fenêtre

        If Not IsNothing(threadServer) Then Me.Dispatcher.Invoke(New dgKill(AddressOf Kill))
        If Not IsNothing(socketClient) Then socketClient.Close()
        If Not IsNothing(threadClient) Then threadClient.Abort()
        If Not IsNothing(threadServer) Then threadServer.Abort()
    End Sub


    ' Activer une note
    Delegate Sub dgEnableNote(index As Integer)
    Private Sub EnableNote(index As Integer)
        imagesList(index).Visibility = Visibility.Visible
    End Sub


    ' Désactiver une note
    Delegate Sub dgDisableNote(ByVal index As Integer)
    Private Sub DisableNote(ByVal index As Integer)
        imagesList(index).Visibility = Visibility.Hidden
    End Sub


    ' Pouvoir faire de façon asynchrone la désactivation des notes (sans bloquer l'UI ou affichage du "Application ne répond pas")
    Private Sub threadDisableNote(index As Integer)
        Thread.Sleep(500)
        Me.Dispatcher.Invoke(New dgDisableNote(AddressOf DisableNote), index)
    End Sub


    ' Activer les boutons et les textboxes
    Delegate Sub dgEnableUI()
    Private Sub EnableUI()
        B_heberger.SetValue(IsEnabledProperty, True)
        B_rejoindre.SetValue(IsEnabledProperty, True)
        TB_ip.SetValue(IsEnabledProperty, True)
        TB_pseudo.SetValue(IsEnabledProperty, True)
    End Sub


    ' Désactiver les boutons et les textboxes
    Delegate Sub dgDisableUI()
    Private Sub DisableUI()
        B_heberger.SetValue(IsEnabledProperty, False)
        B_rejoindre.SetValue(IsEnabledProperty, False)
        TB_ip.SetValue(IsEnabledProperty, False)
        TB_pseudo.SetValue(IsEnabledProperty, False)
    End Sub


    ' Sub appelé quand la fenêtre se ferme
    Private Sub ClientWindow_Closed(sender As Object, e As EventArgs) Handles ClientWindow.Closed
        ' Fermer le serveur s'il est actif (éviter les processus qui bouclent dans le vide)
        Disconnection()
    End Sub


    ' Les 4 méthodes qui suivent gèrent le clic sur chaque note
    Private Sub image2_MouseLeftButtonUp(sender As Object, e As MouseButtonEventArgs) Handles image2.MouseLeftButtonUp
        If canPlay Or verbose Then
            BlinkNote(1)
        End If
    End Sub

    Private Sub image1_MouseLeftButtonUp(sender As Object, e As MouseButtonEventArgs) Handles image1.MouseLeftButtonUp
        If canPlay Or verbose Then
            BlinkNote(0)
        End If
    End Sub

    Private Sub image4_MouseLeftButtonUp(sender As Object, e As MouseButtonEventArgs) Handles image4.MouseLeftButtonUp
        If canPlay Or verbose Then
            BlinkNote(3)
        End If
    End Sub

    Private Sub image3_MouseLeftButtonUp(sender As Object, e As MouseButtonEventArgs) Handles image3.MouseLeftButtonUp
        If canPlay Or verbose Then
            BlinkNote(2)
        End If
    End Sub


    ' Faire clignoter et jouer le son de la note touchée
    Private Sub BlinkNote(i As Integer)
        If canPlay Or verbose Then
            Me.Dispatcher.Invoke(New dgEnableNote(AddressOf EnableNote), i)
            pressedNote(i, True)

            threadSon(i) = New Thread(AddressOf threadDisableNote)
            threadSon(i).Start(i)
        End If
    End Sub


    ' Traiter la note appuyée
    Delegate Sub dgPressedNote(index As Integer)
    Private Sub pressedNote(index As Integer, Optional playing As Boolean = False)
        ' Si le son ciblé était déjà en cours, l'arrêter (sinon : crash de l'application)
        spList(index).Stop()
        spList(index).Play()


        If playing Then
            Dim rightNote As Integer = currentNotesList(currentNote)
            If verbose Then Debug.Print("[GAME] Note pressée : " & index & ". Note attendue : " & rightNote & ".")

            If index <> rightNote Then
                ' La note est différente, c'est perdu !
                ' On envoie le packet pour signifier que le joueur a perdu
                Dim forgePacket As New List(Of String)

                If verbose Then Debug.Print("[GAME] Wrong note -- GAME FAILED")
                forgePacket.Add(PacketType.FAILED)
                forgePacket.Add(PacketType.DUMMY)
                socketClient.Send(NetStrEncode(forgePacket))
                Me.Dispatcher.Invoke(New dgDisconnection(AddressOf Disconnection))
                MsgBox("Vous avez perdu ! Fin de la partie !")
                Me.Dispatcher.Invoke(New dgChangeStatut(AddressOf ChangeStatut), "Partie terminée, vous avez perdu.")

            Else
                ' La note est la bonne, on continue !
                If verbose Then Debug.Print("[GAME] Right note -- Continue")
                currentNote += 1

                If (currentNote >= currentNotesList.Count) Then
                    ' Plus de note à jouer, on envoie le roundwon au serveur
                    canPlay = False
                    Dim forgePacket As New List(Of String)
                    forgePacket.Add(PacketType.ROUNDWON)
                    forgePacket.Add(PacketType.DUMMY)
                    socketClient.Send(NetStrEncode(forgePacket))
                End If
            End If
        End If
    End Sub
End Class
