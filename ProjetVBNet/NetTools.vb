﻿' Bibliothèque de fonctions dédiées à simplifier la communication en réseau du serveur et des clients.
Module NetTools

    ' Convertir un str en Bytes() pour être envoyé sur le réseau
    Private Function NetStrEncode(str As String) As Byte()
        Return System.Text.Encoding.ASCII.GetBytes(str)
    End Function


    ' Convertir une liste de Str en Bytes() avec le caractère séparant le type de données et les données elles-même.
    Function NetStrEncode(strList As List(Of String)) As Byte()
        Dim tempStr As String = ""

        For Each str As String In strList
            If tempStr = "" Then
                tempStr = str
            Else
                tempStr = tempStr & PacketType.SEPCHAR & str
            End If
        Next

        Return NetStrEncode(tempStr)
    End Function


    ' Convertir un Byte() récupéré du réseau en String
    Function NetStrDecode(byt As Byte()) As String
        Return System.Text.Encoding.ASCII.GetString(byt)
    End Function


    ' Convertir une liste de notes en Byte
    Function NetNoteListEncode(noteList As List(Of Integer)) As List(Of String)
        Dim tempStrList As New List(Of String)

        For Each note As Integer In noteList
            tempStrList.Add(note.ToString)
        Next

        Return tempStrList
    End Function


    ' Permet de différencier les différents messages envoyés et reçus par le serveur ou les clients
    Public Class PacketType
        Public Shared SEPCHAR As String = "|"       ' Caractère séparateur dans les packets
        Public Shared DUMMY As String = "♥"         ' Données vides non interprétées
        Public Shared EMPTY As String = vbNullChar  ' Données invalides / vides
        Public Shared ZERO As String = "0"          ' Idem
        Public Shared PSEUDO As Integer = 1         ' Contient le nom du joueur
        Public Shared NOTESLISTE As Integer = 2     ' Contient la liste des notes
        Public Shared FAILED As Integer = 3         ' Savoir si le joueur a perdu son tour ou non
        Public Shared ROUNDWON As Integer = 4       ' Savoir si le joueur a gagné son tour ou non
        Public Shared WAIT As Integer = 5           ' Savoir si c'est le tour du joueur ou non
        Public Shared WAITPLAYER As Integer = 6     ' Signifie à un joueur qu'il attend un autre joueur
        Public Shared PSEUDOADV As Integer = 7      ' Packet contenant le pseudo du joueur adversaire
        Public Shared VICTORY As Integer = 8        ' Savoir si le joueur remporte la partie ou non
        Public Shared FLEEAWAY As Integer = 9       ' Un joueur a quitté la partie en plein jeu
    End Class
End Module