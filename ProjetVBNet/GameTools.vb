﻿' Fonctions communes pouvant être utilisées dans plusieurs fichiers
Module GameTools

    ' Retirer le premier élément d'une liste de String et renvoyer cette même liste...
    ' Pourquoi le VB ne propose-t-il pas cette fonction ? (Array.CopyTo(...) donne un tout autre résultat que celui-ci)
    Function RmFirstItem(strList() As String) As String()
        Dim resultList(strList.Count - 2) As String
        Dim firstItem As Boolean = True

        For i As Integer = 0 To strList.Count - 1
            If i > 0 Then
                resultList(i - 1) = strList(i)
            End If
        Next

        Return resultList
    End Function
End Module
