﻿Imports System.Net.Sockets

Class Client
    Private socketClient As Socket              ' Socket dédié au client
    Private pseudo As String                    ' Pseudo du client
    Private perdu As Boolean = False            ' A-t-il perdu ?
    Public exist As Boolean? = Nothing          ' Client instancié ? (Oui, non ou Nothing). Le "?" signifie que Boolean peut avoir la valeur Nothing

    Dim nbCar As Integer                        ' Nombre de bytes reçus du socket
    Dim forgePacket As New List(Of String)      ' Dédié à la construction d'un packet

    ' Debug
    Dim verbose As Boolean = False


    ' Constructeur de la classe Client
    Sub New(ByVal sock As Socket)
        socketClient = sock
    End Sub


    ' Instanciation du client
    Sub makeClient()
        If verbose Then Debug.Print("[CLIENT] Instanciation d'un client")
        Dim Bytes(255) As Byte      ' 255 bytes maximum seront lus


        ' Premier packet : Pseudo du client
        Try
            nbCar = socketClient.Receive(Bytes)
        Catch ex As Exception
            If verbose Then Debug.Print("[CLIENT] Erreur de réception de pseudo, fermeture du client")
            Return
        End Try

        pseudo = NetStrDecode(Bytes)
        pseudo = pseudo.Replace(vbNullChar, "")     ' Retirer les vbNullChar qui ont rempli l'array

        If Not pseudo = "" Then
            pseudo = pseudo.Split(PacketType.SEPCHAR)(1)
            exist = True
            If verbose Then Debug.Print("[CLIENT] MakeClient : " & pseudo)
            If verbose Then Debug.Print("[CLIENT] pseudo.Count : " & pseudo.Count & " ~ " & pseudo.Length)

            If verbose Then Debug.Print("[CLIENT] MakeClient :  " & pseudo)

            If verbose Then Debug.Print("[CLIENT] Client identifié (" & pseudo & ")")

        Else
            Debug.Print("[CLIENT] Données invalides reçues. Fermeture du client.")
            socketClient.Close()
            exist = False   ' Le client n'existe pas
            Return
        End If


    End Sub


    ' Permet d'envoyer un packet au client désigné
    Sub SendPacket(packet As Byte())
        Dim nbByte As Integer

        Try
            nbByte = socketClient.Send(packet)
            If verbose Then Debug.Print("[ClientThread] " & pseudo & " > " & nbByte & " bytes envoyés")
        Catch ex As Exception
            If verbose Then Debug.Print("[ClientThread] Exception levée lors d'un SendPacket()")
            If verbose Then Debug.Print(">>> " & ex.ToString())
        End Try
    End Sub


    ' Permet de recevoir un packet du client désigné (bloquant)
    Function ReceivePacket() As Byte()
        Dim received(255) As Byte

        Try
            nbCar = socketClient.Receive(received)
            If verbose Then Debug.Print("[CLIENT] Received " & nbCar & " bytes and sent to server")
        Catch ex As Exception
            forgePacket = New List(Of String)
            forgePacket.Add(PacketType.FLEEAWAY)
            forgePacket.Add(PacketType.DUMMY)
            received = NetStrEncode(forgePacket)
        End Try

        Return received
    End Function


    ' Getter pour récupérer le client (à des fins de debug côté serveur)
    Public Function getPseudo() As String
        Try
            Return pseudo
        Catch ex As Exception
            Return "undefined"
        End Try
    End Function


    ' Fermer le socket du client
    Public Sub Close()
        Try
            socketClient.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class